<?php

/**
 * Plugin Name: My Plugin
 * Description: This is a test Plugin
 * Version: 0.1
 * Author: Vaishak 
 * Author URI: https://vaishakj-portfolio.netlify.app/
 */

if (!defined("ABSPATH")) {
    header("Location: /pg");
    die();
}


function my_plugin_activation()
{
    global $wpdb, $table_prefix;
    $wp_emp = $table_prefix . "emp";

    $q = "CREATE TABLE IF NOT EXISTS `$wp_emp` (`ID` INT NOT NULL AUTO_INCREMENT , `school` VARCHAR(50) NOT NULL , `number` VARCHAR(100) NOT NULL ,PRIMARY KEY (`ID`)) ENGINE = MyISAM;";

    $wpdb->query($q);
}
register_activation_hook(__FILE__, "my_plugin_activation");

function my_plugin_deactivation()
{
    global $wpdb, $table_prefix;
    $wp_emp = $table_prefix . "emp";

    $q = "TRUNCATE TABLE `$wp_emp`";
    $wpdb->query($q);
}
register_deactivation_hook(__FILE__, "my_plugin_deactivation");



function my_custom_scripts()
{
    $path_js = plugins_url("js/main.js", __FILE__);
    $path_style = plugins_url("css/style.css", __FILE__);

    $dep = array("jquery");
    $ver = filemtime(plugin_dir_path(__FILE__) . 'js/main.js');
    $ver_style = filemtime(plugin_dir_path(__FILE__) . 'css/style.css');

    wp_enqueue_style("my-custom-style", $path_style, "", $ver_style);
    wp_enqueue_script("my-custom-js", $path_js, $dep, $ver, true);
}

// hook
add_action("wp_enqueue_scripts", "my_custom_scripts");
add_action("admin_enqueue_scripts", "my_custom_scripts");



function pg_a($atts)
{
    global $wpdb, $table_prefix;
    $wp_emp = $table_prefix . "emp";
    $x = $atts['id'];
    $q = "SELECT * FROM `$wp_emp` WHERE ID=$x;";
    $results = $wpdb->get_results($q);

    ob_start();
?>

    <?php


    foreach ($results as $row) :
    ?>
        <!-- <form method="get" action="">
            <a name="whatsappnumber" href="whatsapp/pune" target="_blank">Click to message</a>
        </form> -->
        <!-- <form method="get" action="<?php echo admin_url("admin.php") ?>">
            <a name="phone" href="whatsapp/<?php echo $row->school ?>" target="_blank">Click to message</a>
        </form> -->

        <!-- <a name="whatsappnumber" href="https://wa.me/91<?php echo $row->number ?>" target="_blank">Click to message</a> -->

        <a href="whatsapp/<?php echo $row->school ?>" target="_blank">Click to message</a>

    <?php



    endforeach;

    ?>




<?php
    $html = ob_get_clean();

    return $html;
}
add_shortcode("pg_a", "pg_a");


function pg_func()
{

    include "admin/admin-page.php";
};

function pg_menu()
{
    add_menu_page("PG", "My plugin", "manage_options", "my-plugin-page", "pg_func", "", 6);
};
add_action("admin_menu", "pg_menu");


add_action('init', function () {
    $phones = [
        "whatsapp/pune" => "+910123456789",
    ];
    $url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/');
    if (array_key_exists($url_path, $phones)) {
        $whatsappnumber = $phones[$url_path];
        if (isset($whatsappnumber)) {
            $url = "https://wa.me/$whatsappnumber";
            wp_redirect($url, 301);
        }
        exit();
    }
});
