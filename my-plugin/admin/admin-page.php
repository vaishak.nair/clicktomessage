<?php
global $wpdb, $table_prefix;
$wp_emp = $table_prefix . "emp";
$q = "SELECT * FROM `$wp_emp`;";
$results = $wpdb->get_results($q);




// CREATE
if (isset($_POST["register"])) {
    global $wpdb;
    $school = $wpdb->escape($_POST["school"]);
    $number = $wpdb->escape($_POST["number"]);


    $data = array(
        "school" => $school,
        "number" => $number,
    );
    $wpdb->insert($wp_emp, $data);
}


// DELETE
if (isset($_POST['delete'])) {
    // $id = $_GET["ID"];

    global $wpdb;
    $wpdb->delete($wp_emp, array("id" => $_POST["delete"]));
}




if (isset($_POST["edit"])) {
    $school = $wpdb->escape($_POST["school"]);
    $number = $wpdb->escape($_POST["number"]);
    $id = $wpdb->escape($_POST["id"]);
    $wpdb->update(
        $wp_emp,
        array(
            // "ID" => $id,
            "school" => $school,
            "number" => $number,
        ),
        array(
            "ID" => $id,
        )
    );
};




// UPDATE
if (isset($_GET["id"])) {

    $id = $_GET["id"];


    $query = "SELECT * FROM `$wp_emp` WHERE ID=$id;";
    $result = $wpdb->get_results($query);
    foreach ($result as $row) {

        $id = $row->ID;
        $school = $row->school;
        $number = $row->number;


        ob_start();
?>

        <form action="<?php echo admin_url("admin.php?page=my-plugin-page") ?>" method="post" class="numForm">
            <h1>Enter Updated Details</h1>
            <input type="hidden" value="<?php echo $id ?>" name="id">
            <label for="school">School</label>
            <input type="text" id="school" name="school" value="<?php echo $school ?>">
            <br>
            <br>
            <label for="number">Number</label>
            <input type="text" id="number" name="number" value="<?php echo $number ?>">
            <br>
            <br>
            <button class="button" type="submit" name="edit" id="edit">Update</button>
            <button class=" button"><a href="<?php echo admin_url("admin.php?page=my-plugin-page") ?>">Back</a></button>
        </form>

    <?php
        $html = ob_get_clean();

        echo $html;

        // update($id);
    }
} else {
    ob_start();
    ?>

    <div class="wrap">
        <form action="<?php echo admin_url("admin.php?page=my-plugin-page") ?>" method="post" class="numForm">
            <h1>Enter Details</h1>
            <br>
            <label for="school">School</label>
            <input type="text" id="school" name="school">
            <br>
            <br>
            <label for="number">Number</label>
            <input type="text" id="number" name="number">
            <br>
            <br>
            <button class="button" type="submit" name="register">Submit</button>
        </form>
        <br>
    </div>
    <div class="wrap">
        <table class="wp-list-table widefat fixed striped table-view-list users">
            <thead>
                <tr>
                    <th style="font-weight:600">ID</th>
                    <th style="font-weight:600">School</th>
                    <th style="font-weight:600">Phone</th>

                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($results as $row) :
                ?>
                    <tr>

                        <td><?php echo $row->ID ?></td>
                        <td><?php echo $row->school ?></td>
                        <td><?php echo $row->number ?></td>

                        <!-- <form method="post" action="<?php echo admin_url("admin.php?page=my-plugin-page") ?>"> -->

                        <td><button class="button" name="update" value="<?php echo $row->ID ?>"><a href="<?php echo admin_url("admin.php?page=my-plugin-page&id=" . $row->ID) ?>">UPDATE</a></button></td>

                        <!-- </form> -->

                        <form method="post" action="<?php echo admin_url("admin.php?page=my-plugin-page") ?>">
                            <td>
                                <button class="button" name="delete" value="<?php echo $row->ID ?>">DELETE</button>

                            </td>
                        </form>
                    </tr>
                <?php
                endforeach;
                ?>
            </tbody>
        </table>

    </div>

<?php
    $html = ob_get_clean();

    echo $html;
}
