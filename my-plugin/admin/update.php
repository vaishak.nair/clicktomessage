<?php

ob_start();
?>

<div class="wrap">
    <form action="<?php echo admin_url("admin.php") ?>" method="post" class="numForm">
        <h1>Enter Details</h1>
        <label for="school">School</label>
        <input type="text" id="school" name="school">
        <br>
        <br>
        <label for="number">Number</label>
        <input type="text" id="number" name="number">
        <br>
        <br>
        <button class="button" type="submit" name="register">Submit</button>
    </form>

</div>


<?php
$html = ob_get_clean();

echo $html;
